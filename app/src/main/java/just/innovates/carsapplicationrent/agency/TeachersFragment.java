package just.innovates.carsapplicationrent.agency;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.User;


public class TeachersFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        rootView.findViewById(R.id.tv_cars_add).setVisibility(View.GONE);

        User user = DataBaseHelper.getSavedUser();
        ArrayList<User> users = DataBaseHelper.getDataLists().users;
        ArrayList<User> tempUsers = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).type.equals("teacher") && users.get(i).schoolId.equals(user.id)) {
                tempUsers.add(users.get(i));
            }
        }


        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        SchoolTeachersAdapter carsAdapter = new SchoolTeachersAdapter(tempUsers);
        recyclerView.setAdapter(carsAdapter);

        return rootView;
    }

}