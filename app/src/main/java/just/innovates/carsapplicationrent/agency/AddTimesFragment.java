package just.innovates.carsapplicationrent.agency;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.Calendar;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.TimeItem;
import just.innovates.carsapplicationrent.base.models.User;


public class AddTimesFragment extends Fragment {


    View v;

    EditText from, to;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_time, container, false);


        from = v.findViewById(R.id.from);
        to = v.findViewById(R.id.to);

        selectTimeDialog(from);
        selectTimeDialog(to);

        String id = getArguments().getString("id","");

        v.findViewById(R.id.btn_login_sign_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                User user = DataBaseHelper.findUser(id);

                if(user.times==null)user.times=new ArrayList<>();

                user.times.add(new TimeItem(from.getText().toString(), to.getText().toString()));

                DataBaseHelper.updateUser(user);
                CustomDialogClass alertDialog= new CustomDialogClass(getActivity());
                SweetDialogs.successMessage(alertDialog,
                        "تم الاضافه بنجاح", view1 -> {
                            alertDialog.cancel();
                            alertDialog.dismiss();
                            TimesFragment timesFragment =   new TimesFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("id",id);
                            timesFragment.setArguments(bundle);
                            FragmentHelper.popAllFragments(getActivity());
                            FragmentHelper.addFragment(getActivity(),timesFragment , "TimesFragment");
                        });
            }
        });

        return v;
    }

    int mHour, mMinute;

    private void selectTimeDialog(EditText txtTime) {

        txtTime.setFocusable(false);
        txtTime.setClickable(true);

        txtTime.setOnClickListener((View.OnClickListener) view -> {

            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), (timePicker, hourOfDay, i1) -> {

                String text = ((hourOfDay % 12) < 10 ? "0" : "") + "" + (hourOfDay % 12) + ":00";
                if (hourOfDay >= 12) {
                    text += " ص ";
                } else {
                    text += " م ";
                }

                txtTime.setText(text);


            }, mHour, mMinute, false);
            timePickerDialog.show();
        });

    }


}