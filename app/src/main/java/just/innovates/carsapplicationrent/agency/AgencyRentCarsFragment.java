package just.innovates.carsapplicationrent.agency;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.carsapplicationrent.R;

public class AgencyRentCarsFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        rootView.findViewById(R.id.tv_cars_add).setVisibility(View.INVISIBLE);
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        AgencyRentCarsAdapter agencyRentCarsAdapter = new AgencyRentCarsAdapter(position -> {});
        recyclerView.setAdapter(agencyRentCarsAdapter);

        return rootView;
    }

}