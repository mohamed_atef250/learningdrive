package just.innovates.carsapplicationrent.agency;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class UserTeachersAdapter extends RecyclerView.Adapter<UserTeachersAdapter.ViewHolder> {


    ArrayList<User>users;

    public UserTeachersAdapter(ArrayList<User> users) {
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users==null?0:users.size();
    }


    @Override
    public UserTeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        UserTeachersAdapter.ViewHolder viewHolder = new UserTeachersAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserTeachersAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.imageView,users.get(position).image);

        holder.name.setText(users.get(position).name + " ");
        holder.time.setText(users.get(position).timeItem.from +" : "+users.get(position).timeItem.to);
        holder.phone.setText(users.get(position).phone);
        ConnectionHelper.loadImage(holder.imageView, users.get(position).image);
        CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());
//        holder.delete.setOnClickListener(view -> SweetDialogs.twoButtonDialog(alertDialog,
//                "حذف المدربة"
//                , "هل متاكد من حذف المدربة ؟!",
//                "نعم",
//                "خروج", view1 -> {
//                    alertDialog.cancel();
//                    alertDialog.dismiss();
//                    DataBaseHelper.removeUser(users.get(position));
//                    users.remove(position);
//                    notifyDataSetChanged();
//                    SweetDialogs.singleButtonMessage(view.getContext(), "تم المسح بنجاح");
//                }));

//        holder.times.setOnClickListener(view -> {
//            TimesFragment timesFragment =   new TimesFragment();
//            Bundle bundle = new Bundle();
//            bundle.putString("id",users.get(position).id);
//            timesFragment.setArguments(bundle);
//            FragmentHelper.addFragment(view.getContext(),timesFragment,"TimesFragment");
//        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView phone , name, time,delete;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            phone = view.findViewById(R.id.phone);
            name = view.findViewById(R.id.name);
            time = view.findViewById(R.id.time);
            delete = view.findViewById(R.id.delete);

        }
    }
}