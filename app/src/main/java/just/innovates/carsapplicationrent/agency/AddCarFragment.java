package just.innovates.carsapplicationrent.agency;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.ImageResponse;
import just.innovates.carsapplicationrent.base.filesutils.FileOperations;
import just.innovates.carsapplicationrent.base.filesutils.VolleyFileObject;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;
import just.innovates.carsapplicationrent.volleyutils.ConnectionListener;


public class AddCarFragment extends Fragment {
    private View rootView;

    private EditText id, name, cost, model, color, company, type, engine, year;
    private RelativeLayout selectImage;
    private Button add;
    private ImageView image;
    String addedImage = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_car, container, false);

        id = rootView.findViewById(R.id.id);
        name = rootView.findViewById(R.id.name);
        cost = rootView.findViewById(R.id.cost);
        model = rootView.findViewById(R.id.model);
        color = rootView.findViewById(R.id.color);
        company = rootView.findViewById(R.id.company);
        type = rootView.findViewById(R.id.type);
        engine = rootView.findViewById(R.id.engine);
        year = rootView.findViewById(R.id.year);
        image = rootView.findViewById(R.id.image);
        selectImage = rootView.findViewById(R.id.select_image);

        add = rootView.findViewById(R.id.add);



        image.setOnClickListener(view -> {

             Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره السيارة"), 242);

        });


        selectImage.setOnClickListener(view -> {

             Intent intent;

            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(Intent.createChooser(intent, "اختر صوره السيارة"), 242);

        });


        add.setOnClickListener(view -> {
            Car car = new Car(DataBaseHelper.getSavedAgency(), DataBaseHelper.getCounter() + "", id.getText().toString(), name.getText().toString(), cost
                    .getText().toString(), model.getText().toString(),
                    color.getText().toString(),
                    company.getText().toString(), type.getText().toString()
                    , engine.getText().toString(),
                    year.getText().toString());
            car.setImage(addedImage);
            DataBaseHelper.addCar(car);
            Toast.makeText(getActivity(), "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
            FragmentHelper.addFragment(getActivity(),new CarsFragment(),"CarsFragment");
        });

        return rootView;
    }

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);

            Glide.with(getActivity()).load(Objects.requireNonNull(volleyFileObject)
                    .getCompressObject().getImage()).into(image);

            volleyFileObjects.add(volleyFileObject);



            addServiceApi();
        } catch (Exception E) {
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();
        HashMap<String, String> params = new HashMap<>();
        params.put("teest","test");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                progressDialog.dismiss();
                progressDialog.cancel();
                ImageResponse imageResponse = (ImageResponse) response;
                addedImage = imageResponse.getState();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                progressDialog.dismiss();
                progressDialog.cancel();
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
