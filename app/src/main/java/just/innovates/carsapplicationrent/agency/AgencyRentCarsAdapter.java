package just.innovates.carsapplicationrent.agency;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.OnItemClickListener;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.RentCar;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class AgencyRentCarsAdapter extends RecyclerView.Adapter<AgencyRentCarsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<RentCar> rentCars;


    public AgencyRentCarsAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        rentCars = new ArrayList<>();
        ArrayList<RentCar> tempRentCars = DataBaseHelper.getDataLists().rentCars;
        String id = DataBaseHelper.getSavedAgency().id;
        if(tempRentCars!=null){
            for(int i=0; i<tempRentCars.size(); i++){
                if(id.equals(tempRentCars.get(i).agency.id)){
                    rentCars.add(tempRentCars.get(i));
                }
            }
        }


    }


    @Override
    public int getItemCount() {
        return rentCars.size();
    }


    @Override
    public AgencyRentCarsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rent_agency, parent, false);
        AgencyRentCarsAdapter.ViewHolder viewHolder = new AgencyRentCarsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AgencyRentCarsAdapter.ViewHolder holder, final int position) {
        holder.name.setText("موديل السيارة : " + rentCars.get(position).car.name);
        holder.pricePerDay.setText("سعر الايجار اليوميي : " + rentCars.get(position).car.cost+" ريال سعودي ");
        holder.numberOfDays.setText("عدد ايام الايجار : " + rentCars.get(position).noOfDays);
        holder.haveDriver.setText(" هل يوجد سائق : " + rentCars.get(position).haveDriver);
        holder.haveDelevery.setText(" هل يوجد خدمه توصيل : " + rentCars.get(position).deleivery);
        holder.userName.setText( " اسم العميل : "+ rentCars.get(position).user.name);
        holder.makeCall.setText("الاتصال بالعميل");
        holder.makeCall.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + rentCars.get(position).user.phone));
            view.getContext().startActivity(intent);
        });

        try {
            double totalPrice = Double.parseDouble(rentCars.get(position).car.cost) * Double.parseDouble(rentCars.get(position).noOfDays);
            holder.total.setText(" الاجمالي : " + totalPrice+" ريال سعودي ");
        } catch (Exception e) {
            e.getStackTrace();
        }

        ConnectionHelper.loadImage(holder.image, rentCars.get(position).car.image);
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClickListener(position));
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name, pricePerDay, numberOfDays, haveDriver, haveDelevery, total,userName;
        Button makeCall;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            pricePerDay = view.findViewById(R.id.price_per_day);
            numberOfDays = view.findViewById(R.id.number_of_days);
            haveDriver = view.findViewById(R.id.have_driver);
            haveDelevery = view.findViewById(R.id.have_delevery);
            total = view.findViewById(R.id.total);
            userName = view.findViewById(R.id.user_name);
            makeCall = view.findViewById(R.id.make_call);


        }
    }
}