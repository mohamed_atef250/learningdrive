package just.innovates.carsapplicationrent.agency;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.Agency;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder> {


    private ArrayList<Car> cars;


    public CarsAdapter(Agency agency) {

//        ArrayList<Car> carsTemp = DataBaseHelper.getDataLists().cars;
//
//        if (cars == null) {
//            cars = new ArrayList<>();
//        }
//        if (carsTemp != null) {
//            for (int i = 0; i < carsTemp.size(); i++) {
//                if (carsTemp.get(i).agency.id.equals(agency.id)) {
//                  cars.add(carsTemp.get(i));
//                }
//            }
//        }
    }


    @Override
    public int getItemCount() {
        return 20;
    }


    @Override
    public CarsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher, parent, false);
        CarsAdapter.ViewHolder viewHolder = new CarsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final CarsAdapter.ViewHolder holder, final int position) {

//
//        holder.price.setText(cars.get(position).cost + " ");
//        holder.name.setText(cars.get(position).name + " ");
//        holder.year.setText(cars.get(position).year + " ");
//        holder.engine.setText(cars.get(position).engine + " ");
//        holder.delete.setVisibility(View.VISIBLE);


        ConnectionHelper.loadImage(holder.imageView, "https://www.klma.org/wp-content/uploads/2019/04/%D8%A7%D9%84%D9%85%D8%AF%D8%B1%D8%B3%D8%A9-%D8%A7%D9%84%D8%B3%D8%B9%D9%88%D8%AF%D9%8A%D8%A9-%D9%84%D9%84%D9%82%D9%8A%D8%A7%D8%AF%D8%A9.jpg");

//
//        holder.delete.setOnClickListener(view -> {
//            AlertDialog alertDialog = new AlertDialog.Builder(view.getContext()).create();
//            alertDialog.setTitle("تأكيد الحذف");
//            alertDialog.setMessage("بالتاكيد هل تريد حذف "+cars.get(position).name);
//
//            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "نعم  احذف", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(view.getContext(), "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
//
//                    DataBaseHelper.removeCar(cars.get(position));
//                    cars.remove(position);
//                    notifyDataSetChanged();
//
//                }
//            });
//
//
//
//            alertDialog.show();
//        });
//
//
//
//
        holder.itemView.setOnClickListener(view -> {
            FragmentHelper.addFragment(view.getContext(),new TimesFragment(),"TimesFragment");

        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView price, name, year, engine,delete;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            price = view.findViewById(R.id.price);
            name = view.findViewById(R.id.name);
            year = view.findViewById(R.id.year);
            engine = view.findViewById(R.id.engine);
            delete = view.findViewById(R.id.delete);

        }
    }
}