package just.innovates.carsapplicationrent.agency;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.admin.AddSchoolFragment;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.common.LoginActivity;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;


public class AgencyMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE},342);
        }


        setContentView(R.layout.activity_agency_main);


        SmoothBottomBar smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int position) {
                if(position==0){
                    FragmentHelper.addFragment(AgencyMainActivity.this,new TeachersFragment(),"TeachersFragment");
                }else if(position==1){
                    FragmentHelper.addFragment(AgencyMainActivity.this,new AddTeacherFragment(),"AddTeacherFragment");
                }
                else if(position==2){
                    FragmentHelper.addFragment(AgencyMainActivity.this,new EditCarFragment(),"EditCarFragment");
                }else {
                    finish();
                    startActivity(new Intent(AgencyMainActivity.this, LoginActivity.class));
                }
                return false;
            }
        });






        FragmentHelper.addFragment(this,new TeachersFragment(),"TeachersFragment");






    }
}
