package just.innovates.carsapplicationrent.agency;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.admin.AddSchoolFragment;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.MenuItem;
import just.innovates.carsapplicationrent.base.OnItemClickListener;
import just.innovates.carsapplicationrent.common.LoginActivity;


public class AgencyMenuAdapter extends RecyclerView.Adapter<AgencyMenuAdapter.ViewHolder> {


    ArrayList<MenuItem> menuItems;
    DrawerLayout drawer;
    OnItemClickListener onItemClickListener;

    public AgencyMenuAdapter(ArrayList<MenuItem> menuItems, DrawerLayout drawer, OnItemClickListener onItemClickListener) {
        this.menuItems = menuItems;

        menuItems.add(new MenuItem(R.drawable.ic_home,"الرئيسية"));
        menuItems.add(new MenuItem(R.drawable.ic_orders,"إضافة مدربه"));
        menuItems.add(new MenuItem(R.drawable.ic_orders,"تعديل بيانات المدرسه"));
        menuItems.add(new MenuItem(R.drawable.ic_log_out,"تسجيل خروج"));

        this.drawer = drawer;

        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public int getItemCount() {
        return menuItems.size();
    }


    @Override
    public AgencyMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        AgencyMenuAdapter.ViewHolder viewHolder = new AgencyMenuAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AgencyMenuAdapter.ViewHolder holder, final int position) {

        holder.icon.setImageResource(menuItems.get(position).resourceId);
        holder.title.setText(menuItems.get(position).title);

        holder.itemView.setOnClickListener(view -> {
            AgencyMenuAdapter.this.drawer.closeDrawers();
            if(position==0){
                FragmentHelper.addFragment(view.getContext(),new CarsFragment(),"CarsFragment");
            }else if(position==1){
                FragmentHelper.addFragment(view.getContext(),new AddTeacherFragment(),"AddTeacherFragment");
            }
            else if(position==2){
                FragmentHelper.addFragment(view.getContext(),new AddSchoolFragment(),"AgencyRentCarsFragment");
            }else {

                ((AppCompatActivity)view.getContext()).finish();
                (view.getContext()).startActivity(new Intent(view.getContext(), LoginActivity.class));
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView title;

        public ViewHolder(View view) {
            super(view);

            icon = view.findViewById(R.id.iv_menu_icon);
            title = view.findViewById(R.id.tv_menu_title);
        }
    }
}