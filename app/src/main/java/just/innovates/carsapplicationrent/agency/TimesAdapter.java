package just.innovates.carsapplicationrent.agency;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.Agency;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.base.models.TimeItem;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.ViewHolder> {


    private ArrayList<TimeItem> timeItems;
    String id;

    public TimesAdapter(ArrayList<TimeItem> timeItems, String id) {
        this.timeItems = timeItems;
        this.id = id;
    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public TimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time, parent, false);
        TimesAdapter.ViewHolder viewHolder = new TimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final TimesAdapter.ViewHolder holder, final int position) {

        holder.fromTime.setText(" من الساعه " + timeItems.get(position).from + " ");
        holder.toTime.setText(" الي الساعه " + timeItems.get(position).to + " ");
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());

                SweetDialogs.twoButtonDialog(alertDialog,
                        "حذف المدرسه"
                        , "هل متاكد من حذف المدرسه ؟!",
                        "نعم",
                        "خروج", view1 -> {

                            User user = DataBaseHelper.findUser(id);
                            if (user != null) {
                                alertDialog.cancel();
                                alertDialog.dismiss();
                                timeItems.remove(position);
                                user.times = timeItems;
                                DataBaseHelper.updateUser(user);
                                notifyDataSetChanged();

                                SweetDialogs.singleButtonMessage(view.getContext(), "تم المسح بنجاح");
                            }
                        });
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView fromTime, toTime, delete;

        public ViewHolder(View view) {
            super(view);
            fromTime = view.findViewById(R.id.from_time);
            toTime = view.findViewById(R.id.to_time);
            delete = view.findViewById(R.id.delete);
        }
    }
}