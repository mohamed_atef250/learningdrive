package just.innovates.carsapplicationrent.agency;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class SchoolTeachersAdapter extends RecyclerView.Adapter<SchoolTeachersAdapter.ViewHolder> {


    ArrayList<User>users;

    public SchoolTeachersAdapter(ArrayList<User> users) {
        this.users=users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public SchoolTeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teacher, parent, false);
        SchoolTeachersAdapter.ViewHolder viewHolder = new SchoolTeachersAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SchoolTeachersAdapter.ViewHolder holder, final int position) {


        holder.name.setText(users.get(position).name + " ");
        holder.price.setText(users.get(position).price + " ريال / الساعه ");
        holder.phone.setText(users.get(position).phone);
        ConnectionHelper.loadImage(holder.imageView, users.get(position).image);
        CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());
        holder.delete.setOnClickListener(view -> SweetDialogs.twoButtonDialog(alertDialog,
                "حذف المدربة"
                , "هل متاكد من حذف المدربة ؟!",
                "نعم",
                "خروج", view1 -> {
                    alertDialog.cancel();
                    alertDialog.dismiss();
                    DataBaseHelper.removeUser(users.get(position));
                    users.remove(position);
                    notifyDataSetChanged();
                    SweetDialogs.singleButtonMessage(view.getContext(), "تم المسح بنجاح");
                }));

        holder.times.setOnClickListener(view -> {
            TimesFragment timesFragment =   new TimesFragment();
            Bundle bundle = new Bundle();
            bundle.putString("id",users.get(position).id);
            timesFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(),timesFragment,"TimesFragment");
        });

        holder.customers.setOnClickListener(view -> {
            UserTeachersFragment timesFragment =   new UserTeachersFragment();
            Bundle bundle = new Bundle();
            bundle.putString("id",users.get(position).id);
            timesFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(),timesFragment,"TimesFragment");
        });


    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView phone,price, name, customers, times,delete;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            price = view.findViewById(R.id.price);
            phone = view.findViewById(R.id.phone);
            name = view.findViewById(R.id.name);
            customers = view.findViewById(R.id.customers);
            times = view.findViewById(R.id.times);
            delete = view.findViewById(R.id.delete);

        }
    }
}