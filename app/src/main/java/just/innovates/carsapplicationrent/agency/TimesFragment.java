package just.innovates.carsapplicationrent.agency;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.TimeItem;


public class TimesFragment extends Fragment {
    private View rootView;
    TextView addCars;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        addCars = rootView.findViewById(R.id.tv_cars_add);


        String id = getArguments().getString("id","");


        addCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddTimesFragment addTimesFragment = new AddTimesFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id",id);
                addTimesFragment.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),addTimesFragment,"AddTimesFragment");
            }
        });



        ArrayList<TimeItem> times = new ArrayList<>();

        try {
            times = DataBaseHelper.findUser(id).times;
        }catch (Exception e){
            e.getStackTrace();
        }

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        TimesAdapter carsAdapter = new TimesAdapter(times,id);
        recyclerView.setAdapter(carsAdapter);

        return rootView;
    }

}