package just.innovates.carsapplicationrent.agency;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.ImageResponse;
import just.innovates.carsapplicationrent.base.filesutils.FileOperations;
import just.innovates.carsapplicationrent.base.filesutils.VolleyFileObject;
import just.innovates.carsapplicationrent.base.models.Agency;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;
import just.innovates.carsapplicationrent.volleyutils.ConnectionListener;


public class RegisterAgencyActivity extends AppCompatActivity {

    private EditText officeName, userName, phone, password, email, cpassword;
    private EditText logo, image;
    boolean isLogo = false;
    String selectedLogo = "", selectedImage = "";
    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_agency);

        officeName = findViewById(R.id.office_name);
        userName = findViewById(R.id.user_name);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        register = findViewById(R.id.register);
        email = findViewById(R.id.email);
        logo = findViewById(R.id.logo);
        image = findViewById(R.id.image);
        cpassword = findViewById(R.id.et_login_confirm_password);

        logo.setFocusable(false);
        image.setFocusable(false);


        logo.setOnClickListener(view -> {
            isLogo = true;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);


        });
        image.setOnClickListener(view -> {
            isLogo = false;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);

        });


        register.setOnClickListener(view -> {
            if (!password.getText().toString().equals(cpassword.getText().toString())) {
                Toast.makeText(RegisterAgencyActivity.this, "من فضلك تاكد من تطابق كلمه المرور", Toast.LENGTH_SHORT).show();
            } else {
                Agency agency = new Agency(DataBaseHelper.getCounter() + "", officeName.getText().toString(), userName.getText().toString(), email.getText().toString(), phone.getText().toString(), password.getText().toString());
                agency.setImage(selectedImage);
                agency.setLogo(selectedLogo);
                DataBaseHelper.addAgency(agency);
                DataBaseHelper.saveAgency(agency);
                Toast.makeText(RegisterAgencyActivity.this, "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(RegisterAgencyActivity.this, AgencyMainActivity.class));
            }
        });


    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(this, data, "image",
                            43);

            if (isLogo) {
                logo.setText("تم اختيار شعار المعرض");
            } else {
                image.setText("تم اختيار صورة المعرض");
            }

            volleyFileObjects.add(volleyFileObject);

            addServiceApi();
        } catch (Exception E) {
            Toast.makeText(this, "" + E.getMessage() + " " + E.getStackTrace(), Toast.LENGTH_SHORT).show();
            E.getStackTrace();
        }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();
        HashMap<String, String> params = new HashMap<>();


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                progressDialog.dismiss();
                progressDialog.cancel();

                ImageResponse imageResponse = (ImageResponse) response;

                if (isLogo) {
                    selectedLogo = imageResponse.getState();
                } else {
                    selectedImage = imageResponse.getState();
                }

             }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                progressDialog.dismiss();
                progressDialog.cancel();

            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }


}
