package just.innovates.carsapplicationrent.agency;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;


public class CarsFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        rootView.findViewById(R.id.tv_cars_add).setVisibility(View.GONE);
//        rootView.findViewById(R.id.tv_cars_add).setOnClickListener(view -> {
////            if(DataBaseHelper.getSavedAgency().visiable){
////                FragmentHelper.addFragment(getActivity(),new AddCarFragment(),"AddCarFragment");
////            }else{
////                Toast.makeText(getActivity(), "من فضلك انتظر تفعيل مدير التطبيق", Toast.LENGTH_SHORT).show();
////            }
//
//        });
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        CarsAdapter carsAdapter = new CarsAdapter(DataBaseHelper.getSavedAgency());
        recyclerView.setAdapter(carsAdapter);

        return rootView;
    }

}