package just.innovates.carsapplicationrent.admin;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.common.LoginActivity;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;


public class AdminMainActivity extends AppCompatActivity {
    DrawerLayout drawer;
    RecyclerView menuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, 342);
        }


        setContentView(R.layout.activity_main);
        //  drawer = findViewById(R.id.drawer_layout);
        menuList = findViewById(R.id.menu_list);

        SmoothBottomBar smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int position) {
                if (position == 0) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new AdminSchoolsFragment(), "UserAgenciesFragment");
                } else if (position == 1) {
                    FragmentHelper.addFragment(AdminMainActivity.this, new AddSchoolFragment(), "AgencyRentCarsFragment");
                } else {

                    ((AppCompatActivity) AdminMainActivity.this).finish();
                    (AdminMainActivity.this).startActivity(new Intent(AdminMainActivity.this, LoginActivity.class));
                }
                return false;
            }
        });


//        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
//                drawer.openDrawer(GravityCompat.START);
//        });


        // AdminMenuAdapter menuAdapter = new AdminMenuAdapter(new ArrayList<>(), drawer, position -> {


        // });
        //menuList.setAdapter(menuAdapter);


        FragmentHelper.addFragment(this, new AdminSchoolsFragment(), "AdminAgenciesFragment");


    }
}
