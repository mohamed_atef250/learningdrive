package just.innovates.carsapplicationrent.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.User;

public class AdminSchoolsFragment extends Fragment {
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        rootView.findViewById(R.id.tv_cars_add).setVisibility(View.GONE);
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);

        ArrayList<User>users = DataBaseHelper.getDataLists().users;
        ArrayList<User> tempUsers = new ArrayList<>();
        for(int i=0; i<users.size(); i++){
            if(users.get(i).type.equals("school")) {
                tempUsers.add(users.get(i));
            }
        }

        AdminSchoolsAdapter aganciesAdapter = new AdminSchoolsAdapter(position -> {

        },tempUsers);
        recyclerView.setAdapter(aganciesAdapter);

        return rootView;
    }

}