package just.innovates.carsapplicationrent.admin;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.OnItemClickListener;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class AdminSchoolsAdapter extends RecyclerView.Adapter<AdminSchoolsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    private ArrayList<User> users;


    public AdminSchoolsAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;


    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public AdminSchoolsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_school, parent, false);
        AdminSchoolsAdapter.ViewHolder viewHolder = new AdminSchoolsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminSchoolsAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.logo, users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());
                SweetDialogs.twoButtonDialog(alertDialog,
                        "حذف المدرسه"
                        , "هل متاكد من حذف المدرسه ؟!",
                        "نعم",
                        "خروج", view1 -> {
                            DataBaseHelper.removeUser(users.get(position));
                            users.remove(position);
                            notifyDataSetChanged();
                            SweetDialogs.singleButtonMessage(view.getContext(), "تم المسح بنجاح");
                        });
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView name, delete;

        public ViewHolder(View view) {
            super(view);

            logo = view.findViewById(R.id.logo);
            name = view.findViewById(R.id.name);
            delete = view.findViewById(R.id.delete);


        }
    }
}