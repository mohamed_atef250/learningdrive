package just.innovates.carsapplicationrent.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SelectLocationActivity;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.ImageResponse;
import just.innovates.carsapplicationrent.base.filesutils.FileOperations;
import just.innovates.carsapplicationrent.base.filesutils.VolleyFileObject;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;
import just.innovates.carsapplicationrent.volleyutils.ConnectionListener;

public class AddSchoolFragment extends Fragment {
    private View rootView;
    private TextView addImage;
    private ImageView image;
    private EditText name,address,location,phone,userName,password;
    private Button add;
    private double lat,lng;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_school, container, false);
        addImage = rootView.findViewById(R.id.add_image);
        image = rootView.findViewById(R.id.image);
        name= rootView.findViewById(R.id.name);
        address= rootView.findViewById(R.id.address);
        location= rootView.findViewById(R.id.location);
        phone= rootView.findViewById(R.id.phone);
        userName= rootView.findViewById(R.id.user_name);
        password= rootView.findViewById(R.id.password);

        add= rootView.findViewById(R.id.add);


        add.setOnClickListener(view -> {
            User user = new User(name.getText().toString(),
                    userName.getText().toString(),
                    phone.getText().toString()
            ,address.getText().toString()
            ,password.getText().toString(),selectedImage);
            user.lat=lat;
            user.lng=lng;
            user.type="school";

            DataBaseHelper.addUser(user);

            SweetDialogs.singleButtonMessage(getActivity(), "تم الاضافه بنجاح");
        });

        location.setFocusable(false);
        location.setClickable(true);
        location.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            startActivityForResult(intent, 150);
        });
        addImage.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        return rootView;
    }




    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==150){
            location.setText("تم اختيار الموقع بنجاح");
            lat=data.getDoubleExtra("lat",0.0);
            lng=data.getDoubleExtra("lng",0.0);

        }else {

            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
                            43);


            addImage.setText("تم اضافه الصوره بنجاح");

            volleyFileObjects.add(volleyFileObject);


            addServiceApi();
        }

    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }

}