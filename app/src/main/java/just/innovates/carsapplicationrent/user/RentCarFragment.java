package just.innovates.carsapplicationrent.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.base.models.RentCar;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class RentCarFragment extends Fragment {
    private View rootView;

    private EditText days, driver, delevery;
    private TextView name, price;
    private Button add;
    private ImageView image;

    Car car;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_rent_car, container, false);

        car = (Car) getArguments().getSerializable("car");

        days = rootView.findViewById(R.id.days);
        driver = rootView.findViewById(R.id.driver);
        delevery = rootView.findViewById(R.id.delevery);
        price = rootView.findViewById(R.id.price);
        name = rootView.findViewById(R.id.name);
        image = rootView.findViewById(R.id.image);

        add = rootView.findViewById(R.id.add);


        name.setText(car.name);
        price.setText(car.cost+" ريال سعودي لليوم");
        ConnectionHelper.loadImage(image, car.image);


        driver.setOnClickListener(view -> {
            PopupMenu popUp = null;
            List<String> genderList = new ArrayList<>();
            genderList.add("نعم");
            genderList.add("لا");
            popUp = new PopupMenu(getActivity(), driver);
            for (int i = 0; i < genderList.size(); i++) {
                popUp.getMenu().add(i, i, i, genderList.get(i));
            }

            popUp.setOnMenuItemClickListener(item -> {
                driver.setText(item.getTitle());
                return false;
            });
            popUp.show();
        });

        delevery.setOnClickListener(view -> {
            PopupMenu popUp = null;
            List<String> genderList = new ArrayList<>();
            genderList.add("نعم");
            genderList.add("لا");
            popUp = new PopupMenu(getActivity(), delevery);
            for (int i = 0; i < genderList.size(); i++) {
                popUp.getMenu().add(i, i, i, genderList.get(i));
            }

            popUp.setOnMenuItemClickListener(item -> {
                delevery.setText(item.getTitle());
                return false;
            });
            popUp.show();
        });


        add.setOnClickListener(view -> {
            RentCar rentCar = new RentCar(DataBaseHelper.getSavedUser(), car.agency, car, days.getText().toString(), driver.getText().toString(), delevery.getText().toString());
            DataBaseHelper.addRentCart(rentCar);
            Toast.makeText(getActivity(), "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
        });


        return rootView;
    }


}
