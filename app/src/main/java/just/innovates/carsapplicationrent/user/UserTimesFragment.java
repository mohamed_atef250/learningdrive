package just.innovates.carsapplicationrent.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.agency.AddTimesFragment;
import just.innovates.carsapplicationrent.agency.TimesAdapter;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.TimeItem;


public class UserTimesFragment extends Fragment {
    private View rootView;
    TextView addCars;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        addCars = rootView.findViewById(R.id.tv_cars_add);


        String id = getArguments().getString("id","");


        addCars.setVisibility(View.GONE);



        ArrayList<TimeItem> times = new ArrayList<>();

        try {
            times = DataBaseHelper.findUser(id).times;
        }catch (Exception e){
            e.getStackTrace();
        }

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);
        UserTimesAdapter carsAdapter = new UserTimesAdapter(times,id);
        recyclerView.setAdapter(carsAdapter);

        return rootView;
    }

}