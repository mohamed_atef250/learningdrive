package just.innovates.carsapplicationrent.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class CarDetailsFragment extends Fragment {
    private View rootView;

    private TextView id, name, cost, model, color, company, type, engine, year;

    private Button add;
    private ImageView image;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.activity_car_details, container, false);

        Car carDetails = (Car)getArguments().getSerializable("car");

        id = rootView.findViewById(R.id.id);
        name = rootView.findViewById(R.id.name);
        cost = rootView.findViewById(R.id.cost);
        model = rootView.findViewById(R.id.model);
        color = rootView.findViewById(R.id.color);
        company = rootView.findViewById(R.id.company);
        type = rootView.findViewById(R.id.type);
        engine = rootView.findViewById(R.id.engine);
        year = rootView.findViewById(R.id.year);
        image = rootView.findViewById(R.id.image);


        ConnectionHelper.loadImage(image,carDetails.image);

        name.setText(""+carDetails.name);
        id.setText(""+carDetails.number);
        cost.setText(""+carDetails.cost+" ريال ");
        model.setText(""+carDetails.model);
        color.setText(""+carDetails.color);
        company.setText(""+carDetails.company);
        type.setText(""+carDetails.type);
        engine.setText(""+carDetails.engine);
        year.setText(""+carDetails.year);

        add = rootView.findViewById(R.id.add);




        add.setOnClickListener(view -> {
            RentCarFragment carDetailsFragment = new RentCarFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("car",carDetails);
            carDetailsFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), carDetailsFragment, "RentCarFragment");
        });

        return rootView;
    }





}
