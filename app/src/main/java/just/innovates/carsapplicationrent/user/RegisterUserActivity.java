package just.innovates.carsapplicationrent.user;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.User;


public class RegisterUserActivity extends AppCompatActivity {

    private EditText name, userName, phone, address, password, cpassword;

    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        name = findViewById(R.id.name);
        userName = findViewById(R.id.user_name);
        phone = findViewById(R.id.phone);
        address = findViewById(R.id.address);
        password = findViewById(R.id.password);
        register = findViewById(R.id.register);
        cpassword = findViewById(R.id.et_login_confirm_password);


        register.setOnClickListener(view -> {
            if (!password.getText().toString().equals(cpassword.getText().toString())) {
                Toast.makeText(RegisterUserActivity.this, "من فضلك تاكد من تطابق كلمه المرور", Toast.LENGTH_SHORT).show();
            } else {
                User user = new User(DataBaseHelper.getCounter() + "", name.getText().toString(), userName.getText().toString(), phone.getText().toString(), address.getText().toString(), password.getText().toString());
                user.type="user";
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveUser(user);
                Toast.makeText(RegisterUserActivity.this, "تمت العملية بنجاح", Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(RegisterUserActivity.this, UserMainActivity.class));
            }
        });


    }


}
