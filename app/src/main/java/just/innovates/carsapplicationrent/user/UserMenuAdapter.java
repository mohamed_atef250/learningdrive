package just.innovates.carsapplicationrent.user;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.MenuItem;
import just.innovates.carsapplicationrent.base.OnItemClickListener;
import just.innovates.carsapplicationrent.common.LoginActivity;


public class UserMenuAdapter extends RecyclerView.Adapter<UserMenuAdapter.ViewHolder> {


    ArrayList<MenuItem> menuItems;
    DrawerLayout drawer;
    OnItemClickListener onItemClickListener;

    public UserMenuAdapter(ArrayList<MenuItem> menuItems, DrawerLayout drawer, OnItemClickListener onItemClickListener) {
        this.menuItems = menuItems;

        menuItems.add(new MenuItem(R.drawable.ic_home,"الرئيسية"));
        menuItems.add(new MenuItem(R.drawable.ic_orders,"الطلبات"));
        menuItems.add(new MenuItem(R.drawable.ic_log_out,"تسجيل خروج"));

        this.drawer = drawer;

        this.onItemClickListener = onItemClickListener;

    }


    @Override
    public int getItemCount() {
        return menuItems.size();
    }


    @Override
    public UserMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        UserMenuAdapter.ViewHolder viewHolder = new UserMenuAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserMenuAdapter.ViewHolder holder, final int position) {

        holder.icon.setImageResource(menuItems.get(position).resourceId);
        holder.title.setText(menuItems.get(position).title);

        holder.itemView.setOnClickListener(view -> {
            UserMenuAdapter.this.drawer.closeDrawers();
            if(position==0){
                FragmentHelper.addFragment(view.getContext(),new UserSchoolsFragment(),"UserAgenciesFragment");
            }else if(position==1){
                FragmentHelper.addFragment(view.getContext(),new UserRentCarsFragment(),"AgencyRentCarsFragment");
            }else {
                ((AppCompatActivity)view.getContext()).finish();
                (view.getContext()).startActivity(new Intent(view.getContext(), LoginActivity.class));
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;
        TextView title;

        public ViewHolder(View view) {
            super(view);

            icon = view.findViewById(R.id.iv_menu_icon);
            title = view.findViewById(R.id.tv_menu_title);
        }
    }
}