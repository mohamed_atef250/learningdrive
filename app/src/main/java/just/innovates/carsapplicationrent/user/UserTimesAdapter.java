package just.innovates.carsapplicationrent.user;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.TimeItem;
import just.innovates.carsapplicationrent.base.models.User;


public class UserTimesAdapter extends RecyclerView.Adapter<UserTimesAdapter.ViewHolder> {


    private ArrayList<TimeItem> timeItems;
    String id;

    public UserTimesAdapter(ArrayList<TimeItem> timeItems, String id) {
        this.timeItems = timeItems;
        this.id = id;
    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public UserTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time, parent, false);
        UserTimesAdapter.ViewHolder viewHolder = new UserTimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserTimesAdapter.ViewHolder holder, final int position) {


        holder.fromTime.setText(" من الساعه " + timeItems.get(position).from + " ");
        holder.toTime.setText(" الي الساعه " + timeItems.get(position).to + " ");
        holder.delete.setText("حجز");
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = DataBaseHelper.getSavedUser();
                User teacher = DataBaseHelper.findUser(id);
                user.timeItem = timeItems.get(position);
                if(teacher!=null&&teacher.users==null){
                    teacher.users=new ArrayList<>();
                }

                if(teacher!=null)
                teacher.users.add(user);

                DataBaseHelper.updateUser(teacher);

                SweetDialogs.singleButtonMessage(view.getContext(), "تم الحجز بنجاح");

            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView fromTime, toTime, delete;

        public ViewHolder(View view) {
            super(view);
            fromTime = view.findViewById(R.id.from_time);
            toTime = view.findViewById(R.id.to_time);
            delete = view.findViewById(R.id.delete);
        }
    }
}