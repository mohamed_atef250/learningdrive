package just.innovates.carsapplicationrent.user;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.CustomDialogClass;
import just.innovates.carsapplicationrent.OnItemClickListener;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;


public class UserSchoolsAdapter extends RecyclerView.Adapter<UserSchoolsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    private ArrayList<User> users;


    public UserSchoolsAdapter(OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener = onItemClickListener;
        this.users = users;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public UserSchoolsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_school, parent, false);
        UserSchoolsAdapter.ViewHolder viewHolder = new UserSchoolsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserSchoolsAdapter.ViewHolder holder, final int position) {
        ConnectionHelper.loadImage(holder.logo, users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.chat.setOnClickListener(view -> {

        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserTeachersFragment userTeachersFragment = new UserTeachersFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id",users.get(position).id);
                userTeachersFragment.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),userTeachersFragment,"UserTeachersFragment");

            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView name, chat,phone;

        public ViewHolder(View view) {
            super(view);
            logo = view.findViewById(R.id.logo);
            name = view.findViewById(R.id.name);
            phone = view.findViewById(R.id.phone);
            chat = view.findViewById(R.id.chat);
        }
    }
}