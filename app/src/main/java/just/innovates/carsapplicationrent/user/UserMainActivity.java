package just.innovates.carsapplicationrent.user;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.FragmentHelper;
import just.innovates.carsapplicationrent.common.LoginActivity;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;


public class UserMainActivity extends AppCompatActivity {
    DrawerLayout drawer;
    RecyclerView menuList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE},342);
        }
        
        setContentView(R.layout.activity_user_main);
        FragmentHelper.addFragment(UserMainActivity.this,new UserSchoolsFragment(),"UserTeachersFragment");


        SmoothBottomBar smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener((OnItemSelectedListener) position -> {
            if(position==0){
                FragmentHelper.addFragment(UserMainActivity.this,new UserSchoolsFragment(),"UserTeachersFragment");
            } else {
                ((AppCompatActivity)UserMainActivity.this).finish();
                (UserMainActivity.this).startActivity(new Intent(UserMainActivity.this, LoginActivity.class));
            }
            return false;
        });

        
        
        
       // drawer = findViewById(R.id.drawer_layout);
 


       // FragmentHelper.addFragment(this,new UserAgenciesFragment(),"UserAgenciesFragment");






    }
}
