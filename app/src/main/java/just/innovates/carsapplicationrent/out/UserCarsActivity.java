package just.innovates.carsapplicationrent.out;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.R;

public class UserCarsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list);


        findViewById(R.id.tv_cars_add).setVisibility(View.GONE);
        findViewById(R.id.tv_cars_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserCarsActivity.this, AddCarActivity.class));
            }
        });

        findViewById(R.id.sort).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu typesPopUps;
                typesPopUps = new PopupMenu(UserCarsActivity.this, view);
                int i = 0;
                typesPopUps.getMenu().add(i, i, i, "Best match");
                i = 1;
                typesPopUps.getMenu().add(i, i, i, "Price : Low to High");
                i = 2;
                typesPopUps.getMenu().add(i, i, i, "Price : High to Low");
                i = 3;
                typesPopUps.getMenu().add(i, i, i, "Year : New to Old");
                i = 4;
                typesPopUps.getMenu().add(i, i, i, "Year : Old to New");
                i = 5;
                typesPopUps.getMenu().add(i, i, i, "Last updated");


                typesPopUps.show();

            }
        });


        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        final ArrayList<Integer> images = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            images.add(R.drawable.car2);
            images.add(R.drawable.car1);
            images.add(R.drawable.car4);
            images.add(R.drawable.car3);
        }



    }
}