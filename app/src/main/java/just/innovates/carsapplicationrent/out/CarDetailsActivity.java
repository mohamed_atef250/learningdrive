package just.innovates.carsapplicationrent.out;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.HashMap;

import just.innovates.carsapplicationrent.R;

public class CarDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_car_details);
        SliderLayout mDemoSlider = findViewById(R.id.imageView);
        HashMap<String, String> url_maps = new HashMap<String, String>();
        url_maps.put("Best car ", "https://auto.ndtvimg.com/car-images/big/bmw/x6-m/bmw-x6-m.jpg?v=2");
        url_maps.put("Amazing Car", "https://static.wheels.ae/imgs/5749_1a198549-2020-bmw-x6-28-medium.jpg");
        url_maps.put("Fastest car", "https://138a0e53132475b1dd4e-3f109eb8db5a829f4e5f76453561afcb.ssl.cf1.rackcdn.com/5UXFG2C55E0C44776/2d163fafc400491802a183f2c822dd5d.jpg");
        url_maps.put("Family car", "https://www.bmw.ca/content/bmw/marketCA/bmw_ca/en_CA/all-models/x-series/X6/2019/bmw-x6-inspire/jcr:content/par/highlight/highlightitems/highlightitem_129404686/image/mobile.transform/highlight/image.1561478638771.jpg");


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
            ;

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);


        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CarDetailsActivity.this, RentCarActivity.class));
            }
        });
    }


}