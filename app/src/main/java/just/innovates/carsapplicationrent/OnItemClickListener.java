package just.innovates.carsapplicationrent;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
