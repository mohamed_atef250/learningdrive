package just.innovates.carsapplicationrent.common;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import just.innovates.carsapplicationrent.OnItemClickListener;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.RentCar;


public class RentCarAdapter extends RecyclerView.Adapter<RentCarAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<RentCar> rentCars;


    public RentCarAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.rentCars = DataBaseHelper.getDataLists().rentCars;

        if (rentCars == null) {
            rentCars = new ArrayList<>();
        }

    }


    @Override
    public int getItemCount() {
        return rentCars.size();
    }


    @Override
    public RentCarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rent, parent, false);
        RentCarAdapter.ViewHolder viewHolder = new RentCarAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final RentCarAdapter.ViewHolder holder, final int position) {
        holder.name.setText("موديل السيارة : " + rentCars.get(position).car.name);
        holder.pricePerDay.setText("سعر الايجار اليوميي : " + rentCars.get(position).car.cost);
        holder.numberOfDays.setText("عدد ايام الايجار : " + rentCars.get(position).noOfDays);
        holder.haveDriver.setText(" هل يوجد سائق : " + rentCars.get(position).haveDriver);
        holder.haveDelevery.setText(" هل يوجد خدمه توصيل : " + rentCars.get(position).deleivery);

        try {
            double totalPrice = Double.parseDouble(rentCars.get(position).car.cost) * Double.parseDouble(rentCars.get(position).noOfDays);
            holder.total.setText(" الاجمالي : " + totalPrice);
        } catch (Exception e) {
            e.getStackTrace();
        }
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClickListener(position));
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name, pricePerDay, numberOfDays, haveDriver, haveDelevery, total;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            pricePerDay = view.findViewById(R.id.price_per_day);
            numberOfDays = view.findViewById(R.id.number_of_days);
            haveDriver = view.findViewById(R.id.have_driver);
            haveDelevery = view.findViewById(R.id.have_delevery);
            total = view.findViewById(R.id.total);


        }
    }
}