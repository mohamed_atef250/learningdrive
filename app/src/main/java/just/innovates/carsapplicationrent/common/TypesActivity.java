package just.innovates.carsapplicationrent.common;


import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.agency.RegisterAgencyActivity;
import just.innovates.carsapplicationrent.user.RegisterUserActivity;


public class TypesActivity extends AppCompatActivity {

    TextView userType,OfficeType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);

        userType = findViewById(R.id.register_as_user);
        OfficeType = findViewById(R.id.register_as_office);

        userType.setOnClickListener(view -> {
                startActivity(new Intent(TypesActivity.this, RegisterUserActivity.class));
        });

        OfficeType.setOnClickListener(view -> {
            startActivity(new Intent(TypesActivity.this, RegisterAgencyActivity.class));
        });


    }


}
