package just.innovates.carsapplicationrent.common;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import just.innovates.carsapplicationrent.admin.AdminMainActivity;
import just.innovates.carsapplicationrent.agency.AgencyMainActivity;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.user.UserMainActivity;


public class LoginActivity extends AppCompatActivity {

    EditText emailEditText;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},342);
        }

        emailEditText = findViewById(R.id.et_login_email);
        password = findViewById(R.id.et_login_password);

        findViewById(R.id.tv_login_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

            }
        });
        findViewById(R.id.btn_login_sign_in).setOnClickListener(view -> {


            if (emailEditText.getText().toString().contains("admin") ) {
                startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
            } else {

                User user = DataBaseHelper.findUser(emailEditText.getText().toString(),password.getText().toString());
                DataBaseHelper.saveUser(user);
                if(user!=null) {
                    if (user.type.equals("school")) {
                        startActivity(new Intent(LoginActivity.this, AgencyMainActivity.class));
                    }else if(user.type.equals("user")){
                        startActivity(new Intent(LoginActivity.this, UserMainActivity.class));

                    }
                }else {

                }
            }


        });

    }


}
