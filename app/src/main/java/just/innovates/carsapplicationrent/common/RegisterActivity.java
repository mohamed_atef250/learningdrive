package just.innovates.carsapplicationrent.common;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;

import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.SweetDialogs;
import just.innovates.carsapplicationrent.base.DataBaseHelper;
import just.innovates.carsapplicationrent.base.ImageResponse;
import just.innovates.carsapplicationrent.base.filesutils.FileOperations;
import just.innovates.carsapplicationrent.base.filesutils.VolleyFileObject;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.ConnectionHelper;
import just.innovates.carsapplicationrent.volleyutils.ConnectionListener;


public class RegisterActivity extends AppCompatActivity {
    private TextView addImage;
    private ImageView image;
    private EditText name,address,phone,userName,password;
    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        addImage = findViewById(R.id.add_image);
        image = findViewById(R.id.image);
        name= findViewById(R.id.name);
        address= findViewById(R.id.address);

        phone= findViewById(R.id.phone);
        userName= findViewById(R.id.user_name);
        password= findViewById(R.id.password);

        add= findViewById(R.id.add);


        add.setOnClickListener(view -> {
            User user = new User(name.getText().toString(),
                    userName.getText().toString(),
                    phone.getText().toString()
                    ,address.getText().toString()
                    ,password.getText().toString(),selectedImage);
            user.type="user";

            DataBaseHelper.addUser(user);

            SweetDialogs.singleButtonMessage(this, "تم الاضافه بنجاح");
        });

    }



    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


            volleyFileObjects = new ArrayList<>();
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(this, data, "image",
                            43);


            addImage.setText("تم اضافه الصوره بنجاح");

            volleyFileObjects.add(volleyFileObject);


            addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);
                ConnectionHelper.loadImage(image,selectedImage);
                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



}
