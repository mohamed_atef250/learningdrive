package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class Agency implements Serializable {

    public String id,officeName,userName,logo,image,email,phone,password;
    public boolean visiable=false;

    public Agency(String id, String officeName, String userName, String email, String phone, String password){
        this.id=id;
        this.password =password;
        this.officeName=officeName;
        this.userName=userName;
        this.phone=phone;
        this.email=email;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setVisiable(boolean visiable) {
        this.visiable = visiable;
    }
}
