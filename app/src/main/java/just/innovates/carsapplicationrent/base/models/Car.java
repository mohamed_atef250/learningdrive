package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class Car implements Serializable {

    public String id,number, name, cost, model, color, company, type, engine, year;
    public String image;
    public Agency agency;
    public Car(Agency agency,String id,String number, String name, String cost, String model, String color, String company, String type, String engine, String year) {
        this.id=id;
        this.number=number;
        this.name=name;
        this.cost=cost;
        this.model=model;
        this.color=color;
        this.company=company;
        this.type=type;
        this.engine=engine;
        this.year=year;
        this.agency=agency;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }
}
