package just.innovates.carsapplicationrent.base;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import just.innovates.carsapplicationrent.R;
import just.innovates.carsapplicationrent.agency.AddCarFragment;
import just.innovates.carsapplicationrent.user.UserMenuAdapter;
import just.innovates.carsapplicationrent.user.UserSchoolsFragment;


public class MainActivity extends AppCompatActivity {
    DrawerLayout drawer;
    RecyclerView menuList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CALL_PHONE},342);
        }


        setContentView(R.layout.activity_main);
        //drawer = findViewById(R.id.drawer_layout);
        menuList = findViewById(R.id.menu_list);


        findViewById(R.id.iv_main_menu).setOnClickListener(view -> {
                drawer.openDrawer(GravityCompat.START);
        });


        UserMenuAdapter menuAdapter = new UserMenuAdapter(new ArrayList<>(), drawer, position -> {
        });
        menuList.setAdapter(menuAdapter);


        FragmentHelper.addFragment(this,new UserSchoolsFragment(),"UserAgenciesFragment");






    }
}
