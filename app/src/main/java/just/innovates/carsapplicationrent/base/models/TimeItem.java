package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class TimeItem implements Serializable {
   public String from,to;

   public TimeItem(String from, String to){
       this.from=from;
       this.to=to;
   }
}
