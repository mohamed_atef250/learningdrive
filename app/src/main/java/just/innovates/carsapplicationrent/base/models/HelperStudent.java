package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class HelperStudent implements Serializable {

    public Student student;
    public Helper helper;

    public HelperStudent(Student student, Helper helper) {
        this.student = student;
        this.helper = helper;

    }
}
