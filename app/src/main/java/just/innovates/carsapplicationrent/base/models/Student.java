package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class Student implements Serializable {

    public String id,name,phone,email,password;

    public Student(String id,String name,String phone,String email,String password){
        this.id=id;
        this.name=name;
        this.phone=phone;
        this.email=email;
        this.password =password;
    }
}
