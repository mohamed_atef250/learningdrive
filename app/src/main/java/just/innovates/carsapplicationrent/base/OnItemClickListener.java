package just.innovates.carsapplicationrent.base;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
