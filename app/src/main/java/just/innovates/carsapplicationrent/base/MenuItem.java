package just.innovates.carsapplicationrent.base;

public class MenuItem {
    public int resourceId;
    public String title;

    public MenuItem(int resourceId,String title){
        this.resourceId=resourceId;
        this.title = title;
    }
}
