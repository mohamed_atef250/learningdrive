package just.innovates.carsapplicationrent.base;

 import com.google.gson.annotations.SerializedName;

 public class ImageResponse{

	@SerializedName("state")
	private String state;

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	@Override
 	public String toString(){
		return 
			"ImageResponse{" + 
			"state = '" + state + '\'' + 
			"}";
		}
}