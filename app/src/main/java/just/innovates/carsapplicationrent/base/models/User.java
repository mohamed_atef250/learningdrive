package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;

import just.innovates.carsapplicationrent.base.DataBaseHelper;

public class User implements Serializable {

    public String id,name,userName,phone,address,password,image,type,age,price,schoolId;
    public ArrayList<TimeItem> times;
    public TimeItem timeItem;
    public ArrayList<User> users ;
    public double lat,lng;

    public User(  String name, String userName, String phone,String address,  String password,String image){
        this.id= DataBaseHelper.getCounter()+"";
        this.password =password;
        this.name=name;
        this.userName=userName;
        this.phone=phone;
        this.address=address;
        this.image=image;
        this.times= new ArrayList<>();
        users = new ArrayList<>();
    }


}
