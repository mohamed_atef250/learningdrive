package just.innovates.carsapplicationrent.base;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import java.util.ArrayList;

import just.innovates.carsapplicationrent.base.models.Agency;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.base.models.Helper;
import just.innovates.carsapplicationrent.base.models.HelperStudent;
import just.innovates.carsapplicationrent.base.models.RentCar;
import just.innovates.carsapplicationrent.base.models.Student;
import just.innovates.carsapplicationrent.base.models.User;
import just.innovates.carsapplicationrent.volleyutils.MyApplication;


public class DataBaseHelper {

    private static SharedPreferences sharedPreferences = null;


    private DataBaseHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = context.getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }


    public static void addStudent(Student student) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Student> students = dataLists.students;
        if (students == null) {
            students = new ArrayList<>();
        }
        students.add(student);
        dataLists.students = students;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addCar(Car car) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Car> cars = dataLists.cars;
        if (cars == null) {
            cars = new ArrayList<>();
        }
        cars.add(car);
        dataLists.cars = cars;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addRentCart(RentCar rentCar) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<RentCar> rentCars = dataLists.rentCars;
        if (rentCars == null) {
            rentCars = new ArrayList<>();
        }
        rentCars.add(rentCar);
        dataLists.rentCars = rentCars;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addAgency(Agency agency) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Agency> agencies = dataLists.agencies;
        if (agencies == null) {
            agencies = new ArrayList<>();
        }
        agencies.add(agency);
        dataLists.agencies = agencies;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users == null) {
            users = new ArrayList<>();
        }
        users.add(user);
        dataLists.users = users;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    public static void addHelperStudent(HelperStudent helperStudent) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<HelperStudent> helperStudents = dataLists.helperStudents;
        if (helperStudents == null) {
            helperStudents = new ArrayList<>();
        }
        helperStudents.add(helperStudent);
        dataLists.helperStudents = helperStudents;
        Gson gson = new Gson();
        String json = gson.toJson(dataLists);
        prefsEditor.putString("userDetails", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static User findUser(String id) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id.equals(id) ) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }

    public static Helper findHelper(String id,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<Helper> helpers = dataLists.helpers;
        if (helpers != null) {
            for (int i = 0; i < dataLists.helpers.size(); i++) {
                if (dataLists.helpers.get(i).id.equals(id) && dataLists.helpers.get(i).password.equals(password) ) {
                    return dataLists.helpers.get(i);
                }
            }
        }

        return null;
    }


    public static Student findStudent(String id) {

        DataLists dataLists = getDataLists();
        ArrayList<Student> students = dataLists.students;
        if (students != null) {
            for (int i = 0; i < dataLists.students.size(); i++) {
                if (dataLists.students.get(i).id.equals(id) ) {
                    return dataLists.students.get(i);
                }
            }
        }

        return null;
    }

    public static User findUser(String userName,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {
            for (int i = 0; i < dataLists.users.size(); i++) {

                Log.e("school",users.get(i).userName);

                if (dataLists.users.get(i).userName.equals(userName) &&
                        dataLists.users.get(i).password.equals(password) ) {
                    return dataLists.users.get(i);
                }
            }
        }

        return null;
    }


    public static Agency findAgency(String id,String password) {

        DataLists dataLists = getDataLists();
        ArrayList<Agency> agencies = dataLists.agencies;
        if (agencies != null) {
            for (int i = 0; i < dataLists.agencies.size(); i++) {
                if (dataLists.agencies.get(i).userName.equals(id) &&dataLists.agencies.get(i).password.equals(password) ) {
                    return dataLists.agencies.get(i);
                }
            }
        }

        return null;
    }



    public static void updateUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id.equals(user.id)) {
                    dataLists.users.set(i,user);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }



    public static void updateAgancy(Agency agency) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Agency> agencies = dataLists.agencies;
        if (agencies != null) {

            dataLists.agencies = agencies;

            for (int i = 0; i < dataLists.agencies.size(); i++) {
                if (dataLists.agencies.get(i).id.equals(agency.id)) {
                    dataLists.agencies.get(i).setVisiable(true);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static void removeCar(Car car) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Car> cars = dataLists.cars;
        if (cars != null) {

            dataLists.cars = cars;

            for (int i = 0; i < dataLists.cars.size(); i++) {
                if (dataLists.cars.get(i).id.equals(car.id)) {
                    dataLists.cars.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }

    public static void removeUser(User user) {
        SharedPreferences.Editor prefsEditor =
                getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<User> users = dataLists.users;
        if (users != null) {

            dataLists.users = users;

            for (int i = 0; i < dataLists.users.size(); i++) {
                if (dataLists.users.get(i).id.equals(user.id)) {
                    dataLists.users.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }



    public static void removeStudent(Helper helper) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        DataLists dataLists = getDataLists();
        ArrayList<Helper> helpers = dataLists.helpers;
        if (helpers != null) {
            dataLists.helpers = helpers;
            for (int i = 0; i < dataLists.helpers.size(); i++) {
                if (dataLists.helpers.get(i).id.equals(helper.id)) {
                    dataLists.helpers.remove(i);
                    break;
                }
            }

            Gson gson = new Gson();
            String json = gson.toJson(dataLists);
            prefsEditor.putString("userDetails", json);
            prefsEditor.commit();
            prefsEditor.apply();
        }
    }


    public static User getSavedUser() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getString("savedUser", "");
        return gson.fromJson(json, User.class);
    }

    public static void saveUser(User user) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("savedUser", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }


    public static Agency getSavedAgency() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getString("savedAgency", "");

        return gson.fromJson(json, Agency.class);
    }

    public static void saveAgency(Agency agency) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        Gson gson = new Gson();
        String json = gson.toJson(agency);
        prefsEditor.putString("savedAgency", json);
        prefsEditor.commit();
        prefsEditor.apply();
    }

    
    


    public static DataLists getDataLists() {
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getString("userDetails", "");
        if (json.equals("")) return new DataLists();
        return gson.fromJson(json, DataLists.class);
    }

    public static  int getCounter() {
        int lastCounter = 0;
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();

        try {
            lastCounter = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getInt("counter", 0);
        } catch (Exception e) {
            e.getStackTrace();
        }
        prefsEditor.putInt("counter", lastCounter + 1);
        prefsEditor.commit();
        prefsEditor.apply();
        return  lastCounter+1;
    }


}
