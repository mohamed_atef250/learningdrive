package just.innovates.carsapplicationrent.base;

import java.util.ArrayList;

import just.innovates.carsapplicationrent.base.models.Agency;
import just.innovates.carsapplicationrent.base.models.Car;
import just.innovates.carsapplicationrent.base.models.Helper;
import just.innovates.carsapplicationrent.base.models.HelperStudent;
import just.innovates.carsapplicationrent.base.models.RentCar;
import just.innovates.carsapplicationrent.base.models.Student;
import just.innovates.carsapplicationrent.base.models.User;

public class DataLists {

    public ArrayList<Student> students = new ArrayList<>();
    public ArrayList<Helper> helpers = new ArrayList<>();
    public ArrayList<HelperStudent> helperStudents = new ArrayList<>();
    public ArrayList<Car> cars = new ArrayList<>();
    public ArrayList<Agency> agencies = new ArrayList<>();
    public ArrayList<User> users = new ArrayList<>();
    public ArrayList<RentCar> rentCars = new ArrayList<>();



}
