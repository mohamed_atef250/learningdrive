package just.innovates.carsapplicationrent.base.models;

public class RentCar {

    public User user;
    public Car car;
    public Agency agency;

    public String noOfDays,haveDriver,deleivery;


    public RentCar(User user , Agency agency,Car car,String noOfDays,String haveDriver,String deleivery){
        this.user = user;
        this.agency =agency;
        this.noOfDays=noOfDays;
        this.haveDriver = haveDriver;
        this.deleivery = deleivery;
        this.car=car;

    }

}
