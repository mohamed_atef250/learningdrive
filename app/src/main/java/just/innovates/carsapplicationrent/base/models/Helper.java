package just.innovates.carsapplicationrent.base.models;

import java.io.Serializable;

public class Helper implements Serializable {

    public String id,name,phone,email,password;

    public Helper(String id, String name, String phone, String email, String password){
        this.id=id;
        this.name=name;
        this.phone=phone;
        this.email=email;
        this.password =password;
    }
}
